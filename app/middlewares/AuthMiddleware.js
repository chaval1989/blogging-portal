const jwt = require('express-jwt')
const secret = require('config').secret

const getTokenFromHeaders = (req) => {
    const {headers: { authorization } } = req
    if (authorization && authorization.split(' ')[0] === 'Token') {
        return authorization.split(' ')[1]
    }
    return null
}

const auth = {
    required: jwt({
        secret,
        getToken: getTokenFromHeaders
    }),
    optional: jwt({
        secret,
        getToken: getTokenFromHeaders,
        credentialsRequired: false
    })
}

module.exports = auth
